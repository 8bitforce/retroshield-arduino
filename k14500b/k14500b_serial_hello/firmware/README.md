Erturk's Note:
Yaroslav's repo release included Powershell scripts. I did not include them here.
I created ".\sbc1\mybuild.bat %filename%" instead to compile files.
Sorry, I'm not familiar w/ powershell scripts.


From Yaroslov's repo:

https://github.com/veremenko-y/mc14500-programs
================================================
README.MD

# Collection of programs for MC14500

## Build

Building requires ca65.exe and ld65.exe placed in `cc65/` folder.
Run PowerShell script `./build.ps1`.