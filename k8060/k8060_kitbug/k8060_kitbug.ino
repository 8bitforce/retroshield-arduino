////////////////////////////////////////////////////////////////////
// RetroShield 8060
// 2023/09/29
// Version 0.1
// Erturk Kocalar
//
// The MIT License (MIT)
//
// Copyright (c) 2023 Erturk Kocalar, 8Bitforce.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// Date         Comments                                            Author
// -----------------------------------------------------------------------------
// 09/25/2020   Started implementing Kitbug                         E. Kocalar
// 09/29/2023   Fixed and optimized UART tx/rx                      E. Kocalar

////////////////////////////////////////////////////////////////////
// Options
//   outputDEBUG: Print memory access debugging messages.
////////////////////////////////////////////////////////////////////
#define outputDEBUG     0
#define debugDELAY      1

////////////////////////////////////////////////////////////////////
// include the library code for LCD shield:
////////////////////////////////////////////////////////////////////
#include <avr/pgmspace.h>
#include "pins2_arduino.h"
#include <DIO2.h>

// pinMode2
// DigitalWrite2
// DigitalRead2 are for DIO2 library.

////////////////////////////////////////////////////////////////////
// MEMORY LAYOUT
////////////////////////////////////////////////////////////////////

// 6K MEMORY
#define RAM_START   0x0200
#define RAM_END     0x17FF
byte    RAM[RAM_END-RAM_START+1];

// ROM(s)
#define ROM_KITBUG_START    0x0000
#define ROM_KITBUG_END      (ROM_KITBUG_START+sizeof(rom_kitbug_bin)-1)


////////////////////////////////////////////////////////////////////
// Kitbug Monitor Code
// http://www.moria.de/tech/scmp/software/
// permission by TBD
//
// ; Modified for 600 (SC/MP II 2 MHz) / 1200 (SC/MP II 4 MHz) baud.
// ; Echoing bug that sets the MSB is fixed.
//
// 	.TITLE KITBUG, 'P00937A 12/1/75'
//
////////////////////////////////////////////////////////////////////
// Convert bin to hex at http://tomeko.net/online_tools/file_to_hex.php?lang=en

/*
  !!!!!!!
  PROGMEM requires pgm_read_byte_near()
  !!!!!!!
*/

unsigned char rom_kitbug_bin[]  = {
  // 0x08, 0x08, 0x08, 0x08, 0x90, 0xFA,     // Test code, nop and jump back.
  
  // KITBUG EEPROM
  //   - If you replace this ROM:
  //     see k8060_kitbug_init() which modified PutC  to speed up TX.

0x08, 0x90, 0x1D, 0xC0, 0xFA, 0x01, 0xC0, 0xF2, 0x35, 0xC0, 0xF0, 0x31, 0xC0, 0xEE, 0x36, 0xC0, 
0xEC, 0x32, 0xC0, 0xE4, 0x37, 0xC0, 0xE2, 0x33, 0xC7, 0xFF, 0xC0, 0xE4, 0x07, 0xC0, 0xDF, 0x3F, 
0xC8, 0xDC, 0x06, 0xC8, 0xDB, 0x01, 0xC8, 0xD7, 0x36, 0xC8, 0xD1, 0x32, 0xC8, 0xCF, 0x35, 0xC8, 
0xC9, 0x31, 0xC8, 0xC7, 0x37, 0xC8, 0xC1, 0x33, 0xC8, 0xBF, 0xC4, 0xF6, 0x32, 0xC4, 0x0F, 0x36, 
0xC4, 0x01, 0x37, 0xC4, 0xC8, 0x33, 0xC4, 0x0D, 0x3F, 0xC4, 0x0A, 0x3F, 0xC4, 0x2D, 0x3F, 0xC4, 
0x01, 0x37, 0xC4, 0x85, 0x33, 0x3F, 0x40, 0xE4, 0x47, 0x9C, 0x07, 0x3F, 0xE4, 0x0D, 0x98, 0xA3, 
0x90, 0x6A, 0x40, 0xE4, 0x54, 0x98, 0x09, 0x40, 0xE4, 0x4D, 0x9C, 0x60, 0xC4, 0x00, 0x90, 0x02, 
0xC4, 0x01, 0xCE, 0xFF, 0xC4, 0x00, 0x37, 0xC4, 0xDF, 0x33, 0x3F, 0xE4, 0x0D, 0x9C, 0x4D, 0xC6, 
0x01, 0x35, 0xC6, 0x01, 0x31, 0xC4, 0x01, 0x37, 0xC4, 0xC8, 0x33, 0xC4, 0x0D, 0x3F, 0xC4, 0x0A, 
0x3F, 0x35, 0x01, 0x40, 0x35, 0xC4, 0x01, 0x37, 0xC4, 0x43, 0x33, 0x40, 0x3F, 0x31, 0x01, 0x40, 
0x31, 0x40, 0x3F, 0xC5, 0x01, 0x3F, 0xC2, 0x00, 0x9C, 0xDB, 0xC4, 0x01, 0x37, 0xC4, 0x85, 0x33, 
0x3F, 0xE4, 0x0D, 0x98, 0xD0, 0xE4, 0x15, 0x98, 0x81, 0xC4, 0x00, 0x37, 0xC4, 0xDB, 0x33, 0x3F, 
0xE4, 0x0D, 0x9C, 0x08, 0xC6, 0x01, 0xC6, 0x01, 0xC9, 0xFF, 0x90, 0xB9, 0xC4, 0x01, 0x37, 0xC4, 
0xC8, 0x33, 0xC4, 0x0A, 0x3F, 0xC4, 0x3F, 0x3F, 0xC4, 0x00, 0x90, 0xDB, 0xC4, 0x01, 0x90, 0x02, 
0xC4, 0x00, 0xCA, 0xFB, 0xC4, 0x85, 0x33, 0xCE, 0xFD, 0xC4, 0x01, 0x37, 0xCE, 0xFF, 0xC2, 0xFF, 
0x9C, 0x01, 0x3F, 0xC4, 0x00, 0xCA, 0x03, 0xCA, 0x02, 0x40, 0x03, 0xFC, 0x3A, 0x94, 0x0F, 0x03, 
0xFC, 0xF6, 0x94, 0x19, 0xC6, 0x01, 0x37, 0xC6, 0x01, 0x33, 0x40, 0x3F, 0x90, 0xD2, 0x03, 0xFC, 
0x0D, 0x94, 0xF1, 0x03, 0xFC, 0xFA, 0x94, 0x02, 0x90, 0xEA, 0x02, 0xF4, 0x0A, 0xCA, 0xFF, 0xC4, 
0x04, 0xCA, 0xFE, 0x02, 0xC2, 0x03, 0xF2, 0x03, 0xCA, 0x03, 0xC2, 0x02, 0xF2, 0x02, 0xCA, 0x02, 
0xBA, 0xFE, 0x9C, 0xEF, 0x02, 0xC2, 0x03, 0xF2, 0xFF, 0xCA, 0x03, 0x3F, 0x90, 0xBB, 0xCE, 0xFF, 
0xC4, 0x20, 0x90, 0x04, 0xCE, 0xFF, 0xC4, 0x00, 0xCE, 0xFF, 0xC4, 0xC8, 0x33, 0xCE, 0xFF, 0xC4, 
0x01, 0x37, 0xCE, 0xFF, 0xC4, 0x02, 0xCE, 0xFF, 0xC2, 0x04, 0x1C, 0x1C, 0x1C, 0x1C, 0x02, 0xF4, 
0xF6, 0x94, 0x04, 0xF4, 0x3A, 0x90, 0x02, 0xF4, 0x40, 0x3F, 0xBA, 0x00, 0x98, 0x06, 0xC2, 0x04, 
0xD4, 0x0F, 0x90, 0xEA, 0xC2, 0x03, 0x98, 0x01, 0x3F, 0xC2, 0x01, 0x37, 0xC2, 0x02, 0x33, 0xC6, 
0x04, 0xC6, 0x01, 0x3F, 0x90, 0xB8, 0xC4, 0x08, 0xCA, 0xFF, 0x06, 0xD4, 0x20, 0x9C, 0xFB, 0xC4, 
0xA7, 0x8F, 0x00, 0x06, 0xD4, 0x20, 0x9C, 0xF2, 0x06, 0xDC, 0x01, 0x07, 0xC4, 0x45, 0x8F, 0x01, 
0x06, 0xD4, 0x20, 0x98, 0x02, 0xC4, 0x01, 0xCA, 0xFE, 0x1F, 0x01, 0x1D, 0x01, 0x06, 0xDC, 0x01, 
0xE2, 0xFE, 0x07, 0xBA, 0xFF, 0x9C, 0xE5, 0xC4, 0x45, 0x8F, 0x01, 0x06, 0xD4, 0xFE, 0x07, 0x8F, 
0x04, 0x40, 0xD4, 0x7F, 0x01, 0x40, 0x3F, 0x90, 0xBD, 0x01, 0xC4, 0x25, 0x8F, 0x03, 0x06, 0xDC, 
0x01, 0x07, 0xC4, 0x09, 0xCA, 0xFF, 0xC4, 0x50, 0x8F, 0x01, 0xBA, 0xFF, 0x98, 0x10, 0x40, 0xD4, 
0x01, 0xCA, 0xFE, 0x01, 0x1C, 0x01, 0x06, 0xDC, 0x01, 0xE2, 0xFE, 0x07, 0x90, 0xE8, 0x06, 0xD4, 
0xFE, 0x07, 0xD4, 0x20, 0x98, 0x03, 0x3F, 0x90, 0xD0, 0xC4, 0x00, 0x37, 0xC4, 0x39, 0x33, 0x3F
};



////////////////////////////////////////////////////////////////////
// 8060 Processor Control
////////////////////////////////////////////////////////////////////
//

/* Digital Pin Assignments */
#define DATA_OUT    (PORTL)
#define DATA_IN     (PINL)
#define ADDR_H      (((PINL & 0x0F) << 4) | (PINC & 0x0F))
#define ADDR_L      (PINA)
#define ADDR        ((ADDR_H << 8 | ADDR_L))

#define uP_RDS_N    30
#define uP_SOUT     31
#define uP_WDS_N    32
#define uP_ADS_N    33
#define uP_RESET_N  38
#define uP_SENSE_B  39
#define uP_SENSE_A  40
#define uP_SIN      41
#define uP_FLAG_2   50
#define uP_FLAG_1   51
#define uP_FLAG_0   52
#define uP_CLK      53

// Fast routines to drive clock signals high/low; faster than digitalWrite
// required to meet >100kHz clock
//
#define CLK_HIGH      (PORTB = PORTB | 0b00000001)
#define CLK_LOW       (PORTB = PORTB & 0b11111110)

#define STATE_RDS_N   ((PINC & 0b10000000) >> 7)
#define STATE_SOUT    ((PINC & 0b01000000) >> 6)
#define STATE_WDS_N   ((PINC & 0b00100000) >> 5)
#define STATE_ADS_N   ((PINC & 0b00010000) >> 4)

// I/O pins shifted to bit0
#define STATE_FLAG_0  ((PINB & 0b00000010) >> 1)
#define STATE_FLAG_1  ((PINB & 0b00000100) >> 2)
#define STATE_FLAG_2  ((PINB & 0b00001000) >> 3)
#define STATE_SENSE_A ((PING & 0b00000010) >> 1)
#define STATE_SENSE_B ((PING & 0b00000100) >> 2)


#define SIN_HIGH      (PORTG = PORTG | 0b00000001)
#define SIN_LOW       (PORTG = PORTG & 0b11111110)
#define SENSE_A_HIGH  (PORTG = PORTG | 0b00000010)
#define SENSE_A_LOW   (PORTG = PORTG & 0b11111101)
#define SENSE_B_HIGH  (PORTG = PORTG | 0b00000100)
#define SENSE_B_LOW   (PORTG = PORTG & 0b11111011)

#define FLAG_0_HIGH   (PORTB = PORTB | 0b00000010)
#define FLAG_0_LOW    (PORTB = PORTB & 0b11111101)
#define FLAG_1_HIGH   (PORTB = PORTB | 0b00000100)
#define FLAG_1_LOW    (PORTB = PORTB & 0b11111011)
#define FLAG_2_HIGH   (PORTB = PORTB | 0b00001000)
#define FLAG_2_LOW    (PORTB = PORTB & 0b11110111)

#define DIR_IN        0x00
#define DIR_OUT       0xFF
#define DATA_DIR      DDRL
#define ADDR_H_DIR    DDRC
#define ADDR_L_DIR    DDRA

word clock_cycle_count;

void uP_init()
{
  // Set directions
  DATA_DIR = DIR_IN;
  // DATA_OUT = 0xFF;    // Enable Pull-ups
  
  ADDR_H_DIR = DIR_IN;
  ADDR_L_DIR = DIR_IN;

  pinMode2(uP_CLK,       OUTPUT);
  pinMode2(uP_RESET_N,   OUTPUT);
  pinMode2(uP_RDS_N,     INPUT_PULLUP);
  pinMode2(uP_WDS_N,     INPUT_PULLUP);
  pinMode2(uP_ADS_N,     INPUT_PULLUP);
    
  pinMode2(uP_SOUT,      INPUT_PULLUP);
  pinMode2(uP_SENSE_A,   OUTPUT);          // unsure about reset direction
  pinMode2(uP_SENSE_B,   OUTPUT);          // unsure about reset direction
  pinMode2(uP_SIN,       INPUT_PULLUP);    // unsure about reset direction
  pinMode2(uP_FLAG_0,    INPUT_PULLUP);    // unsure about reset direction
  pinMode2(uP_FLAG_1,    INPUT_PULLUP);    // unsure about reset direction
  pinMode2(uP_FLAG_2,    INPUT_PULLUP);    // unsure about reset direction
  
  digitalWrite2(uP_SENSE_A, LOW);          // Interrupt?
  digitalWrite2(uP_SENSE_B, HIGH);         // UART

  digitalWrite2(uP_CLK, LOW);
  uP_assert_reset();
  
  clock_cycle_count = 0;
}

void uP_assert_reset()
{
  // Drive RESET conditions
  digitalWrite2(uP_RESET_N, LOW);

  // Other pins in default state
  pinMode2(uP_SOUT,      INPUT);
  pinMode2(uP_SENSE_A,   OUTPUT);          // unsure about reset direction
  pinMode2(uP_SENSE_B,   OUTPUT);          // unsure about reset direction
  pinMode2(uP_SIN,       INPUT_PULLUP);    // unsure about reset direction
  pinMode2(uP_FLAG_0,    INPUT_PULLUP);    // unsure about reset direction
  pinMode2(uP_FLAG_1,    INPUT_PULLUP);    // unsure about reset direction
  pinMode2(uP_FLAG_2,    INPUT_PULLUP);    // unsure about reset direction

  digitalWrite2(uP_SENSE_A, HIGH);         // Interrupt?
  digitalWrite2(uP_SENSE_B, HIGH);         // Uart

  ADDR_H_DIR = DIR_IN;
  ADDR_L_DIR = DIR_IN;
}

void uP_release_reset()
{
  // Drive RESET conditions
  digitalWrite2(uP_RESET_N, HIGH);
}


// Modify pipbug ROM for super fast UART
void k8060_kitbug_init()
{
  // TODO

  pinMode2(uP_SENSE_B, OUTPUT);        // Serial output from Arduino to 8060
  pinMode2(uP_FLAG_0,  INPUT_PULLUP);  // Serial output from 8060 to Arduino

  digitalWrite2(uP_SENSE_A, HIGH);    // Interrupt?
  digitalWrite2(uP_SENSE_B, HIGH);    // Uart

  // Modify UART speeds faster than default 300bps
  /*          110 BAUD                    1200 BAUD     2400 BAUD   Modified
              ========                    =========     =========   ========
  0x190 = 57  LDI 87                      0xA7          0x3D      : 0x190
  0x192 = 04  DLY  4    : 2250            0x00          0x00      : 0x192
  0x19D = 7e  LDI 126                     0x45          0x80      : 0x19D
  0x19F = 08  DLY 8     : 4375            0x01          0x00      : 0x19F
  0x1BC = 08  DLY 8                       0x04          0x02      : 0x1C0*
  0x1C7 = ff  LDI 255                       0x25        0x82      : 0x1CB*
  0x1C9 = 17  DLY 23    : large             0x03        0x01      : 0x1CD*
  0x1D3 = 8a  LDI 138                         0x50      0x81      : 0x1D7*
  0x1D5 = 08  DLY 8     : 4375                0x01      0x00      : 0x1D9*
  */

  // Modify KITBUG ROM RX/TX delays for 2400 Baud
  rom_kitbug_bin[ 0x190 ] = 0x3D;
  rom_kitbug_bin[ 0x192 ] = 0x00;
  rom_kitbug_bin[ 0x19d ] = 0x80;
  rom_kitbug_bin[ 0x19f ] = 0x00;
  rom_kitbug_bin[ 0x1c0 ] = 0x02;
  rom_kitbug_bin[ 0x1cb ] = 0x82;
  rom_kitbug_bin[ 0x1cd ] = 0x01;
  rom_kitbug_bin[ 0x1d7 ] = 0x81;
  rom_kitbug_bin[ 0x1d9 ] = 0x00;


  // To speed up UART bitbang, we'll override the TX (PUTC)
  // so that instead of sending it out on FLAG0, we will
  // write the char to PUTC ROM region which Arduino
  // will pick it up and output.

  // RX (GECO) will continue to use bitbanging, b/c
  // it's too much of a hack to implement for now.

  // New PUTC routine:
/*
      PUTC:   ST   0(P0)        // Write to PUTC+1
              JMP  $EXIT
              ...
              ...   // fill w/ NOP's
              ...
      EXIT:   CSA
              ANI   $FE
              CAS
              ANI   0X20
              JZ    $2
              XPPC  P3
              JMP   PUTC
      $2      JS    P3,CMDLP 
*/


#if (1)
  // Fill PUTC w/ NOP's  (4 is the offset in the fixed bin image)
  for(int x=0x1c5+4; x <= (0x1e9+4); x++)
    rom_kitbug_bin[x] = 0x08;     // nop until $EXIT

  // Write tx char to PUTC region for Arduino to pick it up.
  rom_kitbug_bin[ 0x1c9 ] = 0xc8;   // ST 0(PC)
  rom_kitbug_bin[ 0x1ca ] = 0x00;
  rom_kitbug_bin[ 0x1cb ] = 0x90;   // JMP $EXIT
  rom_kitbug_bin[ 0x1cc ] = 0x21;
#endif

}

////////////////////////////////////////////////////////////////////
// Processor Control Loop
////////////////////////////////////////////////////////////////////
// This is where the action is.
// it reads processor control signals and acts accordingly.
//

byte prevADS_N = 1; 
byte prevRDS_N = 1;
byte prevWDS_N = 1;

byte DATA_latched = 0;
word ADDR_latched = 0;

char tmp[80];

inline __attribute__((always_inline))
void cpu_tick()
{   
  CLK_HIGH;
  
  delayMicroseconds(3);

#if (outputDEBUG)
  if ((STATE_ADS_N==0) || (STATE_RDS_N==0) || (STATE_WDS_N==0))
  {
    sprintf(tmp, "%0.4X(H): ADS=%0.1X RDS=%0.1X WDS=%0.1X ADDR=%0.4X D=%0.2X LATCH A=%0.4X D=%0.2X ( ", ADDR_latched, STATE_ADS_N, STATE_RDS_N, STATE_WDS_N, ADDR, DATA_IN, ADDR_latched, DATA_latched);
    Serial.write(tmp);
  }
#endif

  //////////////////////////////////////////////////////////////////////
  // Address Access?
  //////////////////////////////////////////////////////////////////////
  if (STATE_ADS_N == 0)
  {
    // Start latching Address
    ADDR_latched = ADDR;

    if (outputDEBUG) Serial.write(" ADDR ... ");
  }  
  else
  if (STATE_ADS_N & !prevADS_N)
  {
    // Rising edge of ADS_N

    // Nothing to do. Adress already latched.
    if (outputDEBUG) Serial.write(" ADDR Latched. \n");
  }
  
  //////////////////////////////////////////////////////////////////////
  // READ Access?
  //////////////////////////////////////////////////////////////////////  
  // READ - falling edge of RDS_N 
  if (!STATE_RDS_N && prevRDS_N)
  {
    if (outputDEBUG) Serial.write(" READ FALLING. ");

    // KITBUG ROM @0000
    if ( (ROM_KITBUG_START <= ADDR_latched) && (ADDR_latched <= ROM_KITBUG_END) )
    {
      // DATA_latched = pgm_read_byte_near(rom_kitbug_bin + (ADDR_latched - ROM_KITBUG_START));
      DATA_latched = rom_kitbug_bin[(ADDR_latched - ROM_KITBUG_START)];
    } else
    if ( (RAM_START <= ADDR_latched) && (ADDR_latched <= RAM_END) )
    {
      DATA_latched = RAM[ADDR_latched - RAM_START];
    } else    
    {
      // Unknown address:
      DATA_latched = 0xFF;

      if (true || outputDEBUG) { Serial.write(" !!! Unknown Read "); Serial.print(ADDR_latched, HEX); Serial.write("\n"); }
    }

    // Output data to processor
    DATA_DIR = DIR_OUT;
    DATA_OUT = DATA_latched;
    if (outputDEBUG) { Serial.write(" DATA ==>> "); Serial.print(DATA_latched, HEX); Serial.write(" "); }

    // if (ADDR_latched == 0x1C9)
    // {
    //   sprintf(tmp, ">> PUTC\n");
    //   // sprintf(tmp, "%0.4X(H): ADS=%0.1X RDS=%0.1X WDS=%0.1X ADDR=%0.4X D=%0.2X LATCH A=%0.4X D=%0.2X \n", ADDR_latched, STATE_ADS_N, STATE_RDS_N, STATE_WDS_N, ADDR, DATA_IN, ADDR_latched, DATA_latched);
    //   Serial.write(tmp);
    // }
    // else
    // if (ADDR_latched == 0x1F6)
    // {
    //   sprintf(tmp, "-- PUTC\n");
    //   Serial.write(tmp);

    // }

  }
  else
  // READ - RDS Low
  if (!STATE_RDS_N)
  {
    //Continue to drive data; processor will latch on rising edge of RDS_N
    
    DATA_DIR = DIR_OUT;
    DATA_OUT = DATA_latched;  

    if (outputDEBUG) { Serial.write(" DATA = "); Serial.print(DATA_latched, HEX); Serial.write(" "); }
  }
  else
  //////////////////////////////////////////////////////////////////////
  // WRITE Access?
  //////////////////////////////////////////////////////////////////////  
  // WRITE - WDS low
  if (!STATE_WDS_N)
  {
    // continue to latch data from processor until WDS_N rising edge
    DATA_DIR = DIR_IN;
    // DATA_OUT = 0xFF;    // ?? Disable Pull-ups
    DATA_latched = DATA_IN;

    if (outputDEBUG) { Serial.write(" WRITE <<== "); Serial.print(DATA_latched, HEX); Serial.write(" "); }
  }
  else
  // WRITE - rising edge
  if (STATE_WDS_N && !prevWDS_N)
  {
    // process data latched
    if (outputDEBUG) { Serial.write(" WRITE Latched = "); Serial.print(DATA_latched, HEX); Serial.write(" \n"); }

    if ( (RAM_START <= ADDR_latched) && (ADDR_latched <= RAM_END) )
    {
      RAM[ADDR_latched - RAM_START] = DATA_latched;
    }
    else
    if ( ADDR_latched == 0x1ca)
    {
      // Hacked PUTC routine outputting char:
      // Serial.write(" PUTC = "); Serial.print(DATA_latched, HEX); Serial.write(" \n");
      Serial.write(DATA_latched);
    }
    else
    {
      if (true || outputDEBUG) { Serial.write(" !!! Unknown Write "); Serial.print(ADDR_latched, HEX); Serial.write("\n"); }
    }
  }

  // Capture previous states for edge detection
  prevADS_N = STATE_ADS_N;
  prevRDS_N = STATE_RDS_N;
  prevWDS_N = STATE_WDS_N;

#if outputDEBUG
    delay(debugDELAY);
    if (outputDEBUG) 
    {
      if ((STATE_ADS_N & STATE_RDS_N & STATE_WDS_N) == 0)
        Serial.write("\n");
    }
#endif

  //////////////////////////////////////////////////////////////////////
  // start next cycle

  CLK_LOW;
  delayMicroseconds(3);

  // turn databus to input if 8060 is not using the bus.
  if (STATE_RDS_N & STATE_WDS_N)
  {
    DATA_DIR = DIR_IN;
    // DATA_OUT = 0xFF;    // Enable Arduino Pull-ups
  }

#if (0 && outputDEBUG)
  if ((STATE_ADS_N & STATE_RDS_N & STATE_WDS_N) == 0)
  {
    sprintf(tmp, "%0.4X(L): ADS=%0.1X RDS=%0.1X WDS=%0.1X ADDR=%0.4X D=%0.2X LATCH A=%0.4X D=%0.2X ( ", ADDR_latched, STATE_ADS_N, STATE_RDS_N, STATE_WDS_N, ADDR, DATA_IN, ADDR_latched, DATA_latched);
    Serial.write(tmp);
    Serial.write("\n");
  }
#endif

#if outputDEBUG
  delay(debugDELAY);
#endif

  // clock_cycle_count ++;
}

////////////////////////////////////////////////////////////////////
// Serial Event
// Soft-UART for 8060's FLAG_0/SENSE_B
////////////////////////////////////////////////////////////////////

// #cpu cycles per baud - measure and update here.
// Make sure it is EVEN for 1.5x.
// #define SOFT_UART_BAUD    (3324)     // 1200 Baud
#define SOFT_UART_BAUD    (1660)        // 2400 Baud

#define PIN_ARDUINO_TXD   (uP_SENSE_B)
#define PIN_ARDUINO_RXD   ( 1-(STATE_FLAG_0))

byte rxd;
word rxd_delay    = 0;  // start capturing 1.5 bits later, middle
byte rxd_bit      = 0;
byte rxd_bit_prev = 1;  // used for falling edge detect

byte txd;
word txd_delay = 0;         // start output 1 bit at a time
byte txd_bit   = 0;


bool serial_available = false;
unsigned int serial_check_delay = 0;

inline __attribute__((always_inline))
void serialEvent8060()
{
  // replicate inverted serial output from SC/MP II
  // digitalWrite2(6, PIN_ARDUINO_RXD);


  // Don't check serial on every visit - slow.
  if (serial_check_delay == 0)
  {
    serial_available = Serial.available();
    serial_check_delay = SOFT_UART_BAUD<<4; // 25000;
    // serial_available will trigger a transmit sequence. we'll have to clear it on the next iteration, else {} section.
    // we should be able to transmit in 256 cycles.
  } else {
    serial_available = false;       // so we don't continously transmit teh same byte over and over again.
    serial_check_delay--;
  }
  
  // Arduino transmits to CPU
  if ( (txd_bit == 0) && (serial_available) )
  {
    txd_bit = 9;
    txd = toupper( Serial.read() );
    txd_delay = SOFT_UART_BAUD;  // 192;

    pinMode2(PIN_ARDUINO_TXD, OUTPUT);
    digitalWrite2(PIN_ARDUINO_TXD, LOW);      // Start bit, low
  }
  else
  if (txd_bit >0)
  {
    txd_delay--;
    if (txd_delay == 0)
    {
      digitalWrite2(PIN_ARDUINO_TXD, (txd & 0x01));
      txd = (txd >> 1);
      txd_delay = SOFT_UART_BAUD;  // 192;

      // are we done yet?  1bit left, which is stop bit
      txd_bit--;
      if (txd_bit == 0x01)
      {
        // set bit0 to output stop bit
        txd = 0x01;
      }
      //else  // already set to 0 on last bit by stop bit
      //if (txd_bit == 0)
      //  digitalWrite2(PIN_ARDUINO_TXD, HIGH);   
    }
  }

  // Arduino receives from CPU
  // Check for start bit
  if (rxd_bit == 0 && (PIN_ARDUINO_RXD == 0) && (rxd_bit_prev == 1))
  {
    rxd_bit  = 8;   // need to receive 8(data)+1(stop) bits.. Ignore stop bit - messes w/ shifting
    rxd = 0;   // OR incoming bits to this.
    rxd_delay = SOFT_UART_BAUD + (SOFT_UART_BAUD>>1);  // wait until first bit middle - 1.5x ... 288
  }
  else
  if (rxd_bit >0)
  {
    rxd_delay--;
    if (rxd_delay == 0)
    {
      // digitalWrite2(7, !digitalRead2(7));    // toggle gpio 7 to indicate we are latching
      
      // rxd = rxd | (0b10000000 * PIN_ARDUINO_RXD);    // shift into MSB
      rxd = rxd | (PIN_ARDUINO_RXD << 7);
      rxd_delay = SOFT_UART_BAUD;  // 192;

      rxd_bit--;
      // are we done yet?
      if (rxd_bit == 0x00)
      {
        rxd = rxd & 0x7F;        
        Serial.write(rxd); // Serial.write(" ("); Serial.print(rxd, HEX); Serial.write(") "); 
        // Serial.write(rxd);
        // no more bits to receive.
        // stop bit will be ignored.
      }
      rxd = (rxd >> 1);
    }
  }  
  // save previous state for edge detection
  rxd_bit_prev = PIN_ARDUINO_RXD;
}

////////////////////////////////////////////////////////////////////
// Setup
////////////////////////////////////////////////////////////////////

void setup() 
{

  Serial.begin(115200);

  Serial.write(27);       // ESC command
  Serial.print("[2J");    // clear screen command
  Serial.write(27);
  Serial.print("[H");
  Serial.println("\n");
  Serial.println("Configuration:");
  Serial.println("==============");
  Serial.print("Debug:      "); Serial.println(outputDEBUG, HEX);
  Serial.print("ROM Size:   "); Serial.print(ROM_KITBUG_END - ROM_KITBUG_START + 1, DEC); Serial.println(" Bytes");
  Serial.print("SRAM Size:  "); Serial.print(RAM_END - RAM_START + 1, DEC); Serial.println(" Bytes");
  Serial.print("SRAM_START: 0x"); Serial.println(RAM_START, HEX); 
  Serial.print("SRAM_END:   0x"); Serial.println(RAM_END, HEX); 
  Serial.println("");
  Serial.println("=======================================================");
  Serial.println("> KITBUG: A Tiny monitor code for SC/MP");
  Serial.println(">   Thanks to Michael for ROM binary image.");
  Serial.println(">   http://www.moria.de/tech/scmp/software/");
  Serial.println("> ");
  Serial.println("=======================================================");
  Serial.println("");
  

  // pinMode2(7, OUTPUT);
  // digitalWrite2(7, HIGH);
  // pinMode2(6, OUTPUT);
  // digitalWrite2(6, HIGH);
  // while(1);

  // Initialize processor GPIO's
  uP_init();
  k8060_kitbug_init();

  // Serial.println("\n");

  // Reset processor
  //
  Serial.println("RESET=0");
  uP_assert_reset();
  for(int i=0;i<25;i++)
    cpu_tick();
  
  // Go, go, go
  uP_release_reset();
  Serial.println("RESET=1");
}

////////////////////////////////////////////////////////////////////
// Loop()
////////////////////////////////////////////////////////////////////

void loop()
{
  word i = 1;
  word j = 0;
  
  // Loop forever
  //
  while(1)
  {    
    //////////////////////////////
    cpu_tick();
    serialEvent8060();     // handles soft-uart on FLAGE/SENSE.

    #define MEASURE_BAUD_RATE 0
    #if MEASURE_BAUD_RATE
    if ( (STATE_FLAG_0))
    {
      if (j != 0)
      {
        Serial.print("BAUD = "); Serial.println(j);
        j = 0;        
      }
    }
    else
    {
      j++;
    }
    #endif
      
  }
}
