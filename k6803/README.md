# RetroShield 6803 for Arduino Mega

These are the software project folders for the RetroShield 6803

* `k6803_billbug`: [Bill Beech's 6800 Monitor](http://www.nj7p.info/Computers/Software/Mon.html) - based on Motorola's MiniBug
* `k6803_test`: bring-up & validation of RetroShield 6803.
