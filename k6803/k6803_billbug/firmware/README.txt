These tools were written by William Beech, NJ7P, over the period 1978 to the present time.
These tools and source code are released into the public domain under the GPL version 2 
license.  They are released with no warranty of any kind.  They are constantly under
development and surely contain bugs.  If you discover a bug, please notify me at nj7p at nj7p
dot info.

These tools were compiled, using the makefile and batch files included, with the MinGW tools 
under windows or with the standard Linux tools on Linux.

Bill Beech, 
9 Oct 2011