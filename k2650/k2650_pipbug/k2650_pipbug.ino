////////////////////////////////////////////////////////////////////
// RetroShield 2650
// 2019/08/26
// Version 0.1
// Erturk Kocalar
//
// The MIT License (MIT)
//
// Copyright (c) 2019 Erturk Kocalar, 8Bitforce.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// Date         Comments                                            Author
// -----------------------------------------------------------------------------
// 8/26/2019    Initial Bring-up.                                   E. Kocalar
//
////////////////////////////////////////////////////////////////////
// Options
//   USE_SPI_RAM: Enable Microchip 128KB SPI-RAM  (Details coming up)
//   USE_LCD_KEYPAD: Enable LCD/Keyboard Shield
//   outputDEBUG: Print memory access debugging messages.
////////////////////////////////////////////////////////////////////
#define USE_LCD_KEYPAD  0
#define outputDEBUG     0

////////////////////////////////////////////////////////////////////
// include the library code for LCD shield:
////////////////////////////////////////////////////////////////////
#include <avr/pgmspace.h>
#include "pins2_arduino.h"
#include <DIO2.h>

////////////////////////////////////////////////////////////////////
// Configuration
////////////////////////////////////////////////////////////////////
#if USE_LCD_KEYPAD

#include <LiquidCrystal.h>

  /*
    The circuit:
   * LCD RS pin to digital pin 12
   * LCD Enable pin to digital pin 11
   * LCD D4 pin to digital pin 5
   * LCD D5 pin to digital pin 4
   * LCD D6 pin to digital pin 3
   * LCD D7 pin to digital pin 2
   * LCD R/W pin to ground
   * 10K resistor:
   * ends to +5V and ground
   * wiper to LCD VO pin (pin 3)
  */

  #define LCD_RS  8
  #define LCD_EN  9
  #define LCD_D4  4
  #define LCD_D5  5
  #define LCD_D6  6
  #define LCD_D7  7
  #define LCD_BL  10
  #define LCD_BTN  0
  
  #define NUM_KEYS   5
  #define BTN_DEBOUNCE 10
  #define BTN_RIGHT  0
  #define BTN_UP     1
  #define BTN_DOWN   2
  #define BTN_LEFT   3
  #define BTN_SELECT 4
  const int adc_key_val[NUM_KEYS] = { 30, 180, 360, 535, 760 };
  int       key = -1;
  int       oldkey = -1;
  boolean   BTN_PRESS = 0;
  boolean   BTN_RELEASE = 0;

  LiquidCrystal lcd(LCD_RS, LCD_EN, LCD_D4, LCD_D5, LCD_D6, LCD_D7);
  int backlightSet = 25;
#endif

////////////////////////////////////////////////////////////////////
// 2650 DEFINITIONS
////////////////////////////////////////////////////////////////////

// 2650 HW CONSTRAINTS
// !!! TODO !!!
//

////////////////////////////////////////////////////////////////////
// MEMORY LAYOUT
////////////////////////////////////////////////////////////////////

// 6K MEMORY
#define RAM_START   0x0400
#define RAM_END     0x1BFF
byte    RAM[RAM_END-RAM_START+1];

// ROM(s) (Monitor)
#define ROM_START   0x0000
#define ROM_END     (ROM_START+sizeof(rom_bin)-1)

////////////////////////////////////////////////////////////////////
// Monitor Code
// PIP BUG
// written by Signetics.
// downloaded from Craig Southeren's 18up5 repo
// https://bitbucket.org/postincrement/18up5-2650-remake/src/master/
////////////////////////////////////////////////////////////////////
// Convert bin to hex at http://tomeko.net/online_tools/file_to_hex.php?lang=en

unsigned char rom_bin[] = {
0x07, 0x3F, 0x20, 0xCF, 0x44, 0x00, 0x5B, 0x7B, 0x04, 0x77, 0xCC, 0x04, 0x09, 0x04, 0x1B, 0xCC, 
0x04, 0x0B, 0x04, 0x80, 0xCC, 0x04, 0x0C, 0x1B, 0x09, 0x01, 0x60, 0x01, 0x6E, 0x04, 0x3F, 0x3F, 
0x02, 0xB4, 0x75, 0xFF, 0x3F, 0x00, 0x8A, 0x04, 0x2A, 0x3F, 0x02, 0xB4, 0x3B, 0x2D, 0x20, 0xCC, 
0x04, 0x27, 0x0C, 0x04, 0x13, 0xE4, 0x41, 0x1C, 0x00, 0xAB, 0xE4, 0x42, 0x1C, 0x01, 0xE5, 0xE4, 
0x43, 0x1C, 0x01, 0xCA, 0xE4, 0x44, 0x1C, 0x03, 0x10, 0xE4, 0x47, 0x1C, 0x01, 0x3A, 0xE4, 0x4C, 
0x1C, 0x03, 0xB5, 0xE4, 0x53, 0x1C, 0x00, 0xF4, 0x1F, 0x00, 0x1D, 0x07, 0xFF, 0xCF, 0x04, 0x27, 
0xE7, 0x14, 0x18, 0x19, 0x3F, 0x02, 0x86, 0xE4, 0x7F, 0x98, 0x0E, 0xE7, 0xFF, 0x18, 0x71, 0x0F, 
0x64, 0x13, 0x3F, 0x02, 0xB4, 0xA7, 0x01, 0x1B, 0x67, 0xE4, 0x0D, 0x98, 0x18, 0x05, 0x01, 0x03, 
0x1A, 0x02, 0x85, 0x02, 0xCD, 0x04, 0x2A, 0xCF, 0x04, 0x29, 0x04, 0x0D, 0x3F, 0x02, 0xB4, 0x04, 
0x0A, 0x3F, 0x02, 0xB4, 0x17, 0x05, 0x02, 0xE4, 0x0A, 0x18, 0x64, 0xCF, 0x24, 0x13, 0x3F, 0x02, 
0xB4, 0x1F, 0x00, 0x60, 0xCD, 0x04, 0x0D, 0xCE, 0x04, 0x0E, 0x17, 0x3F, 0x02, 0xDB, 0x3B, 0x74, 
0x3F, 0x02, 0x69, 0x0D, 0x04, 0x0E, 0x3F, 0x02, 0x69, 0x3F, 0x03, 0x5B, 0x0D, 0x84, 0x0D, 0x3F, 
0x02, 0x69, 0x3F, 0x03, 0x5B, 0x3F, 0x00, 0x5B, 0x0C, 0x04, 0x2A, 0xE4, 0x02, 0x1E, 0x00, 0x22, 
0x18, 0x11, 0xCC, 0x04, 0x11, 0x3F, 0x02, 0xDB, 0xCE, 0x84, 0x0D, 0x0C, 0x04, 0x11, 0xE4, 0x04, 
0x9C, 0x00, 0x22, 0x06, 0x01, 0x8E, 0x04, 0x0E, 0x05, 0x00, 0x77, 0x08, 0x8D, 0x04, 0x0D, 0x75, 
0x08, 0x1F, 0x00, 0xAE, 0x3F, 0x02, 0xDB, 0xE6, 0x08, 0x1D, 0x00, 0x1D, 0xCE, 0x04, 0x11, 0x0E, 
0x64, 0x00, 0xC1, 0x3F, 0x02, 0x69, 0x3F, 0x03, 0x5B, 0x3F, 0x00, 0x5B, 0x0C, 0x04, 0x2A, 0xE4, 
0x02, 0x1E, 0x00, 0x22, 0x18, 0x1C, 0xCC, 0x04, 0x0F, 0x3F, 0x02, 0xDB, 0x02, 0x0E, 0x04, 0x11, 
0xCE, 0x64, 0x00, 0xE6, 0x08, 0x98, 0x03, 0xCC, 0x04, 0x0A, 0x0C, 0x04, 0x0F, 0xE4, 0x03, 0x1C, 
0x00, 0x22, 0x0E, 0x04, 0x11, 0x86, 0x01, 0x1F, 0x00, 0xF7, 0x3F, 0x02, 0xDB, 0x3F, 0x00, 0xA4, 
0x0C, 0x04, 0x07, 0x92, 0x0D, 0x04, 0x01, 0x0E, 0x04, 0x02, 0x0F, 0x04, 0x03, 0x77, 0x10, 0x0D, 
0x04, 0x04, 0x0E, 0x04, 0x05, 0x0F, 0x04, 0x06, 0x0C, 0x04, 0x00, 0x75, 0xFF, 0x1F, 0x04, 0x09, 
0xCC, 0x04, 0x00, 0x13, 0xCC, 0x04, 0x08, 0xCC, 0x04, 0x0A, 0x04, 0x00, 0x1B, 0x0C, 0xCC, 0x04, 
0x00, 0x13, 0xCC, 0x04, 0x08, 0xCC, 0x04, 0x0A, 0x04, 0x01, 0xCC, 0x04, 0x11, 0x12, 0xCC, 0x04, 
0x07, 0x77, 0x10, 0xCD, 0x04, 0x04, 0xCE, 0x04, 0x05, 0xCF, 0x04, 0x06, 0x75, 0x18, 0xCD, 0x04, 
0x01, 0xCE, 0x04, 0x02, 0xCF, 0x04, 0x03, 0x0E, 0x04, 0x11, 0x3B, 0x0F, 0x0D, 0x04, 0x0D, 0x3F, 
0x02, 0x69, 0x0D, 0x04, 0x0E, 0x3F, 0x02, 0x69, 0x1F, 0x00, 0x22, 0x20, 0xCE, 0x64, 0x2D, 0x0E, 
0x64, 0x33, 0xCC, 0x04, 0x0D, 0x0E, 0x64, 0x35, 0xCC, 0x04, 0x0E, 0x0E, 0x64, 0x2F, 0xCC, 0x84, 
0x0D, 0x0E, 0x64, 0x31, 0x07, 0x01, 0xCF, 0xE4, 0x0D, 0x17, 0x3B, 0x0B, 0x0E, 0x64, 0x2D, 0x1C, 
0x00, 0x1D, 0x3B, 0x57, 0x1F, 0x00, 0x22, 0x3F, 0x02, 0xDB, 0xA6, 0x01, 0x1E, 0x02, 0x50, 0xE6, 
0x01, 0x1D, 0x02, 0x50, 0x17, 0x3B, 0x70, 0x0E, 0x64, 0x2D, 0xBC, 0x01, 0xAB, 0xCE, 0x04, 0x11, 
0x3F, 0x02, 0xDB, 0x3F, 0x00, 0xA4, 0x0F, 0x04, 0x11, 0x02, 0xCF, 0x64, 0x35, 0x01, 0xCF, 0x64, 
0x33, 0x0C, 0x84, 0x0D, 0xCF, 0x64, 0x2F, 0x05, 0x9B, 0xCD, 0x84, 0x0D, 0x06, 0x01, 0x0E, 0xE4, 
0x0D, 0xCF, 0x64, 0x31, 0x0F, 0x62, 0x22, 0xCE, 0xE4, 0x0D, 0x04, 0xFF, 0xCF, 0x64, 0x2D, 0x1F, 
0x00, 0x22, 0x99, 0x9B, 0x3F, 0x02, 0x86, 0x3B, 0x1D, 0xD3, 0xD3, 0xD3, 0xD3, 0xCF, 0x04, 0x12, 
0x3F, 0x02, 0x86, 0x3B, 0x11, 0x6F, 0x04, 0x12, 0x03, 0xC1, 0x3B, 0x01, 0x17, 0x01, 0x2C, 0x04, 
0x2C, 0xD0, 0xCC, 0x04, 0x2C, 0x17, 0x07, 0x10, 0xEF, 0x42, 0x59, 0x14, 0xE7, 0x01, 0x9A, 0x78, 
0x0C, 0x04, 0x07, 0x64, 0x40, 0x92, 0x1F, 0x00, 0x1D, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 
0x37, 0x38, 0x39, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0xCD, 0x04, 0x12, 0x3B, 0x4F, 0x51, 0x51, 
0x51, 0x51, 0x45, 0x0F, 0x0D, 0x62, 0x59, 0x3F, 0x02, 0xB4, 0x0D, 0x04, 0x12, 0x45, 0x0F, 0x0D, 
0x62, 0x59, 0x3F, 0x02, 0xB4, 0x17, 0x77, 0x10, 0x04, 0x80, 0xB0, 0x05, 0x00, 0x06, 0x08, 0x12, 
0x1A, 0x74, 0x20, 0xB0, 0x3B, 0x17, 0x3B, 0x10, 0x12, 0x44, 0x80, 0x51, 0x61, 0xC1, 0xFA, 0x76, 
0x3B, 0x06, 0x45, 0x7F, 0x01, 0x75, 0x18, 0x17, 0x20, 0xF8, 0x7E, 0xF8, 0x7E, 0xF8, 0x7E, 0x04, 
0xE5, 0xF8, 0x7E, 0x17, 0x77, 0x10, 0x76, 0x40, 0xC2, 0x05, 0x08, 0x3B, 0x6B, 0x3B, 0x69, 0x74, 
0x40, 0x3B, 0x65, 0x52, 0x1A, 0x04, 0x74, 0x40, 0x1B, 0x02, 0x76, 0x40, 0xF9, 0x73, 0x3B, 0x58, 
0x76, 0x40, 0x75, 0x10, 0x17, 0x0C, 0x04, 0x2A, 0x18, 0x07, 0x17, 0x20, 0xC1, 0xC2, 0xCC, 0x04, 
0x2A, 0x0F, 0x04, 0x27, 0xEF, 0x04, 0x29, 0x14, 0x0F, 0x24, 0x13, 0xCF, 0x04, 0x27, 0xE4, 0x20, 
0x18, 0x63, 0x3F, 0x02, 0x46, 0x04, 0x0F, 0xD2, 0xD2, 0xD2, 0xD2, 0x42, 0xD1, 0xD1, 0xD1, 0xD1, 
0x45, 0xF0, 0x46, 0xF0, 0x61, 0xC1, 0x03, 0x62, 0xC2, 0x04, 0x01, 0xCC, 0x04, 0x2A, 0x1B, 0x51, 
0x3B, 0x49, 0x3F, 0x00, 0xA4, 0x3B, 0x44, 0x86, 0x01, 0x77, 0x08, 0x85, 0x00, 0x75, 0x08, 0xCD, 
0x04, 0x0F, 0xCE, 0x04, 0x10, 0x3B, 0x38, 0x04, 0xFF, 0xCC, 0x04, 0x29, 0x3F, 0x00, 0x8A, 0x04, 
0x3A, 0x3F, 0x02, 0xB4, 0x20, 0xCC, 0x04, 0x2C, 0x0D, 0x04, 0x0F, 0x0E, 0x04, 0x10, 0xAE, 0x04, 
0x0E, 0x77, 0x08, 0xAD, 0x04, 0x0D, 0x75, 0x08, 0x1E, 0x00, 0x1D, 0x19, 0x1C, 0x5A, 0x1C, 0x07, 
0x04, 0x3F, 0x02, 0x69, 0xFB, 0x7B, 0x3B, 0x07, 0x1F, 0x00, 0x22, 0x07, 0x03, 0x1B, 0x02, 0x07, 
0x32, 0x04, 0x20, 0x3F, 0x02, 0xB4, 0xFB, 0x79, 0x17, 0x06, 0xFF, 0xCE, 0x04, 0x28, 0x0D, 0x04, 
0x0D, 0x3F, 0x02, 0x69, 0x0D, 0x04, 0x0E, 0x3F, 0x02, 0x69, 0x0D, 0x04, 0x28, 0x3F, 0x02, 0x69, 
0x0D, 0x04, 0x2C, 0x3F, 0x02, 0x69, 0x0F, 0x04, 0x29, 0x0F, 0xA4, 0x0D, 0xEF, 0x04, 0x28, 0x18, 
0x09, 0xCF, 0x04, 0x29, 0xC1, 0x3F, 0x02, 0x69, 0x1B, 0x6C, 0x0D, 0x04, 0x2C, 0x3F, 0x02, 0x69, 
0x0E, 0x04, 0x0E, 0x8E, 0x04, 0x28, 0x05, 0x00, 0x77, 0x08, 0x8D, 0x04, 0x0D, 0x75, 0x08, 0x3F, 
0x00, 0xA4, 0x1F, 0x03, 0x25, 0x3F, 0x02, 0x86, 0xE4, 0x3A, 0x98, 0x79, 0x20, 0xCC, 0x04, 0x2C, 
0x3F, 0x02, 0x24, 0xCD, 0x04, 0x0D, 0x3F, 0x02, 0x24, 0xCD, 0x04, 0x0E, 0x3F, 0x02, 0x24, 0x59, 
0x03, 0x1F, 0x84, 0x0D, 0xCD, 0x04, 0x28, 0x3F, 0x02, 0x24, 0x0C, 0x04, 0x2C, 0x9C, 0x00, 0x1D, 
0xC3, 0xCF, 0x04, 0x29, 0x3F, 0x02, 0x24, 0x0F, 0x04, 0x29, 0xEF, 0x04, 0x28, 0x18, 0x06, 0x01, 
0xCF, 0xE4, 0x0D, 0xDB, 0x6C, 0x0C, 0x04, 0x2C, 0x9C, 0x00, 0x1D, 0x1F, 0x03, 0xB5, 0x00, 0x00
};


////////////////////////////////////////////////////////////////////
// 2650 Processor Control
////////////////////////////////////////////////////////////////////
//

/* Digital Pin Assignments */
#define DATA_OUT (PORTL)
#define DATA_IN  (PINL)
#define ADDR_H   (PINA & 0x7F)
#define ADDR_L   (PINC)
#define ADDR     ((ADDR_H << 8 | ADDR_L))

#define uP_CLK      52
#define uP_RESET    40
#define uP_MIO_N    53
#define uP_RW_N     51
#define uP_OPREQ    50
#define uP_INTREQ_N 41
#define uP_INTACK   39
#define uP_FLAG     29
#define uP_SENSE    38

// Fast routines to drive clock signals high/low; faster than digitalWrite
// required to meet >100kHz clock
//
#define CLK_HIGH      (PORTB = PORTB | 0b00000010)
#define CLK_LOW       (PORTB = PORTB & 0b11111101)
#define STATE_MIO_N   (PINB & 0x01)
// WATCHOUT: R is active-low and W is active high
#define STATE_RW_N    (PINB & 0x04)
#define STATE_OPREQ   (PINB & 0x08)
#define STATE_INTACK  (PING & 0x04)
#define STATE_FLAG    (PINA & 0x80)

#define SENSE_HIGH    (PORTD = PORTD | 0x80)
#define SENSE_LOW     (PORTD = PORTD & 0x7F)

#define DIR_IN     0x00
#define DIR_OUT    0xFF
#define DATA_DIR   DDRL
#define ADDR_H_DIR DDRA
#define ADDR_L_DIR DDRC

unsigned long clock_cycle_count;
unsigned long clock_cycle_last;
unsigned long uP_start_millis;
unsigned long uP_stop_millis;
unsigned long uP_millis_last;

word uP_ADDR;
byte uP_DATA;

void uP_init()
{
  // Set directions
  DATA_DIR = DIR_IN;
  DATA_OUT = 0xFF;    // Enable Pull-ups
  
  ADDR_H_DIR = DIR_IN;
  ADDR_L_DIR = DIR_IN;

  pinMode(uP_CLK,       OUTPUT);
  pinMode(uP_RESET,     OUTPUT);
  pinMode(uP_INTREQ_N,  OUTPUT);
  pinMode(uP_SENSE,     OUTPUT);
    
  pinMode(uP_MIO_N,     INPUT);
  pinMode(uP_RW_N,      INPUT);
  pinMode(uP_OPREQ,     INPUT);
  pinMode(uP_INTACK,    INPUT);
  pinMode(uP_FLAG,      INPUT_PULLUP);

  digitalWrite(uP_CLK, LOW);
  uP_assert_reset();
  
  clock_cycle_count = 0;
  clock_cycle_last  = 0;
  uP_start_millis   = millis();
  uP_millis_last    = millis();

}

void uP_assert_reset()
{
  digitalWrite(uP_INTREQ_N, HIGH);
  digitalWrite(uP_SENSE,    HIGH);     

  // Drive RESET conditions
  digitalWrite(uP_RESET, HIGH);
}

void uP_release_reset()
{
  // Drive RESET conditions
  digitalWrite(uP_RESET, LOW);
}

// Modified DLAY & DLY
// 0x3B, 0x06, 0x45, 0x7F, 0x01, 0x75, 0x18, 0x17, 0x20, 0x04, 0x20, 0xF8, 0x7E, 0xC0, 0xC0, 0x04, 
// 0x05, 0xF8, 0x7E, 0x17, 0x77, 0x10, 0x76, 0x40, 0xC2, 0x05, 0x08, 0x3B, 0x6B, 0x3B, 0x69, 0x74, 

// Modify pipbug ROM for super fast UART
void k2650_pipbug_init()
{
  // Modify DLAY and DLY subroutines to count downto 0x20 and 0x05.
  rom_bin[0x02A9 - 0x0000] = 0x04;    // LODI,R0 H'20'
  rom_bin[0x02AA - 0x0000] = 0x20;
  
  rom_bin[0x02AD - 0x0000] = 0xC0;    // NOP
  rom_bin[0x02AE - 0x0000] = 0xC0;    // NOP

  rom_bin[0x02AF - 0x0000] = 0x04;    // LODI,R0 H'05'
  rom_bin[0x02B0 - 0x0000] = 0x05;
}
////////////////////////////////////////////////////////////////////
// Processor Control Loop
////////////////////////////////////////////////////////////////////
// This is where the action is.
// it reads processor control signals and acts accordingly.
//

bool currRD = 0;
bool prevRD = 0;
bool currWR_N = 1; 
bool prevWR_N = 1;
byte DATA_latched = 0;
word ADDR_latched = 0;

inline __attribute__((always_inline))
void cpu_tick()
{   
  CLK_HIGH;
  uP_ADDR   = ADDR;

  currRD   = STATE_OPREQ && STATE_MIO_N && !STATE_RW_N;
  // digitalWrite2(7, currRD);
  currWR_N = !(STATE_OPREQ && STATE_MIO_N && STATE_RW_N);
  // digitalWrite2(6, currWR_N);
  
  if (0) 
  {    
    {
      char tmp[40];
      sprintf(tmp, "/ ADDR=%0.4X D=%0.2X", uP_ADDR, DATA_IN);
      Serial.write(tmp);
    }
    
    Serial.print(" OPREQ: ");
    Serial.print(STATE_OPREQ, HEX);
  
    Serial.print(" M/IO#: ");
    Serial.print(STATE_MIO_N, HEX);
  
    Serial.print(" RD: ");
    Serial.print(currRD, HEX);
    Serial.print(" WR_N: ");
    Serial.print(currWR_N, HEX);
  
    //Serial.print(" INTACK: ");
    //Serial.print(STATE_INTACK, HEX);
  
    //Serial.print(" FLAG: ");
    //Serial.print(STATE_FLAG, HEX);
    
    //Serial.print(" prevRD: ");
    //Serial.print(prevRD, HEX);
    //Serial.print(" prevWR_N: ");
    //Serial.print(prevWR_N, HEX);
  
    Serial.println(" ");
  }
  
  //////////////////////////////////////////////////////////////////////
  // Memory Access?
  //////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////
  // WR_N
  ////////////////////////////////////////////////////////////
  // Start capturing data_in but don't write it to destination yet
  if (!currWR_N)
  {
    // Serial.println("M: Data_latched");
    DATA_DIR = DIR_IN;
    DATA_latched = DATA_IN;
    ADDR_latched = uP_ADDR;
  } 
  else
  ////////////////////////////////////////////////////////////
  // WR_N Rising Edge
  ////////////////////////////////////////////////////////////
  // Write data to destination when WR# goes high.
  if (!prevWR_N && currWR_N)
  {
    // use ADDR_latched because ADDR may not be valid anymore.
    
    // Serial.println("M: Data_written");
    // Memory Write
    if ( (RAM_START <= ADDR_latched) && (ADDR_latched <= RAM_END) )
      RAM[ADDR_latched - RAM_START] = DATA_latched;

#if 0 // outputDEBUG
    if ((0x0400 <= ADDR_latched) && (ADDR_latched <= 0x0435)) 
    {
      char tmp[20];
      sprintf(tmp, "WR A=%0.4X D=%0.2X\n", uP_ADDR, DATA_latched);
      Serial.write(tmp);
    }
#endif

  }      
  // else     // -- FIXME
  ////////////////////////////////////////////////////////////
  // RD Rising Edge
  ////////////////////////////////////////////////////////////
  if (!prevRD && currRD)     // Rising edge of RD
  {
    // Serial.println("READING...");
    
    // change DATA port to output to uP:
    DATA_DIR = DIR_OUT;

    // ROM?
    if ( (ROM_START <= uP_ADDR) && (uP_ADDR <= ROM_END) )
      DATA_latched = rom_bin [(uP_ADDR - ROM_START)];
    else
    // Execute from RAM?
    if ( (RAM_START <= uP_ADDR) && (uP_ADDR <= RAM_END) )
      DATA_latched = RAM[uP_ADDR - RAM_START];
    else
      DATA_latched = 0xFF;

    DATA_OUT = DATA_latched;

#if 0     // outputDEBUG
    if ((0x0400 <= uP_ADDR) && (uP_ADDR <= 0x0435)) 
    {
      char tmp[40];
      sprintf(tmp, "-- A=%0.4X D=%0.2X\n", uP_ADDR, DATA_latched);
      Serial.write(tmp);
    }
    if ((0x005B <= uP_ADDR) && (uP_ADDR <= 0x00A3)) 
    {
      char tmp[40];
      sprintf(tmp, "-- A=%0.4X D=%0.2X\n", uP_ADDR, DATA_latched);
      Serial.write(tmp);
    }
#endif
  } 
  else
  ////////////////////////////////////////////////////////////
  // RD_N
  ////////////////////////////////////////////////////////////
  if (currRD)    // Continue to output data read on falling edge ^^^
  {
    // Serial.println("Reading continue.");
    
    DATA_DIR = DIR_OUT;

    DATA_OUT = DATA_latched;
  } 

  else

  //////////////////////////////////////////////////////////////////////
  // IO Access?
  //////////////////////////////////////////////////////////////////////
  if (STATE_OPREQ && !STATE_MIO_N && false)
  {    
    ////////////////////////////////////////////////////////////
    // WR_N
    ////////////////////////////////////////////////////////////
    // Start capturing data_in but don't write it to destination yet
    if (!STATE_RW_N)
    {
      DATA_latched = DATA_IN;
    } 
    else
    // IO Write?
    if (STATE_RW_N && !prevWR_N)      // perform write on rising edge
    {

#if (outputDEBUG)
      {
        char tmp[40];
        sprintf(tmp, "IORQ WR#=%0.1X A=%0.4X D=%0.2X\n", STATE_RW_N, uP_ADDR, DATA_latched);
        Serial.write(tmp);
      }
#endif
      
    } 
    else
    // IO Read?
    if (STATE_RW_N && !prevRD)    // perform actual read on rising edge
    {
      // change DATA port to output to uP:
      DATA_DIR = DIR_OUT;

      DATA_latched = 0xFF;

      DATA_OUT = DATA_latched;
      
#if (outputDEBUG)
      {
        char tmp[40];
        sprintf(tmp, "IORQ RD#=%0.1X A=%0.4X D=%0.2X\n", STATE_RW_N, uP_ADDR, DATA_latched);
        Serial.write(tmp);
      }
#endif
      
    } 
    else
    // continuing IO Read?
    if (STATE_RW_N)    // continue output same data
    {
      // change DATA port to output to uP:
      DATA_DIR = DIR_OUT;

      DATA_OUT = DATA_latched;
    } 

  }

  // Capture previous states for edge detection
  prevRD   = currRD;
  prevWR_N = currWR_N;

#if outputDEBUG
    delay(10);
#endif

  //////////////////////////////////////////////////////////////////////
  // start next cycle

  CLK_LOW;    // E goes low

#if (USE_LCD_KEYPAD)
  // one full cycle complete
  clock_cycle_count ++;
#endif

  // turn databus to input if 2650 is not reading from ROM/RAM/IO.
  if (!STATE_OPREQ) // || !currWR_N)
  {
    DATA_DIR = DIR_IN;
    DATA_OUT = 0xFF;    // Enable Arduino Pull-ups
  }

  if (0)
  {
    {
      char tmp[40];
      sprintf(tmp, "\\ ADDR=%0.4X D=%0.2X", uP_ADDR, DATA_IN);
      Serial.write(tmp);
    }

    Serial.print(" OPREQ: ");
    Serial.print(STATE_OPREQ, HEX);
  
    Serial.print(" M/IO#: ");
    Serial.print(STATE_MIO_N, HEX);
  
    Serial.print(" RD: ");
    Serial.print(currRD, HEX);
    Serial.print(" WR_N: ");
    Serial.print(currWR_N, HEX);
  
    Serial.print(" INTACK: ");
    Serial.print(STATE_INTACK, HEX);
  
    Serial.print(" FLAG: ");
    Serial.print(STATE_FLAG, HEX);
    
    //Serial.print(" prevRD: ");
    //Serial.print(prevRD, HEX);
    //Serial.print(" prevWR_N: ");
    //Serial.print(prevWR_N, HEX);
  
    Serial.println(" ");
  }
  
#if outputDEBUG
    delay(10);
#endif
}

////////////////////////////////////////////////////////////////////
// Serial Event
////////////////////////////////////////////////////////////////////

////////////////////////////////////////
// Soft-UART for 2650's FLAG/SENSE
////////////////////////////////////////

// #define k2650_UART_BAUD (9045)
#define k2650_UART_BAUD (423)
byte txd_2650;
word txd_delay = k2650_UART_BAUD*1.5;     // start capturing 1.5 bits later, middle
byte txd_bit = 0;

byte rxd_2650;
word rxd_delay = k2650_UART_BAUD;         // start output 1 bit at a time
byte rxd_bit = 0;

inline __attribute__((always_inline))
void serialEvent2650()
{
  // RXD
  if (rxd_bit == 0 && Serial.available())
  {
    rxd_bit = 9;
    rxd_2650 = toupper( Serial.read() );
    if (rxd_2650 == '\\')
      rxd_2650 = 0x0A;
    rxd_delay = k2650_UART_BAUD;  // 192;

    pinMode2(uP_SENSE, OUTPUT);
    digitalWrite2(uP_SENSE, LOW);      // Start bit, low
  }
  else
  if (rxd_bit)
  {
    rxd_delay--;
    if (rxd_delay == 0)
    {
      digitalWrite2(uP_SENSE, (rxd_2650 & 0x01));
      rxd_2650 = (rxd_2650 >> 1);
      rxd_delay = k2650_UART_BAUD;  // 192;

      // are we done yet?  1bit left, which is stop bit
      rxd_bit--;
      if (rxd_bit == 0x01)
      {
        // set bit0 to output stop bit
        rxd_2650 = 0x01;
      }
      else
      if (rxd_bit == 0)
        digitalWrite2(uP_SENSE, HIGH);   
    }
  }

  // TXD
  // Check for start bit
  if (txd_bit == 0 && !STATE_FLAG)
  {
    txd_bit  = 9;   // need to receive 8(data)+1(stop) bits
    txd_2650 = 0;   // OR incoming bits to this.
    txd_delay = 1.5*k2650_UART_BAUD;  // 288
  }
  else
  if (txd_bit)
  {
    txd_delay--;
    if (txd_delay == 0)
    {
      // digitalWrite2(7, HIGH);
      txd_2650 = (txd_2650 >> 1) | (STATE_FLAG);
      // digitalWrite2(7, LOW);
      txd_delay = k2650_UART_BAUD;  // 192;

      // are we done yet?  1bit left, which is stop bit
      if ((--txd_bit) == 0x01)
      {
        Serial.write(txd_2650);
        // no more bits to receive.
        // stop bit will be ignored.
      }
    }
  }  
}

////////////////////////////////////////////////////////////////////
// LCD/Keyboard functions
////////////////////////////////////////////////////////////////////

#if (USE_LCD_KEYPAD)

////////////////////////////////////////////////////////////////////
// int getKey() - LCD/Keyboard function from vendor
////////////////////////////////////////////////////////////////////

int getKey()
{
  key = get_key2();
  if (key != oldkey)
    {
      delay(BTN_DEBOUNCE);
      key = get_key2();
      if (key != oldkey) {
        oldkey = key;
        if (key == -1)
          BTN_RELEASE = 1;
        else
          BTN_PRESS = 1;
      }
    } else {
      BTN_PRESS = 0;
      BTN_RELEASE = 0;
    }
  return (key != -1);
}

int get_key2()
{
  int k;
  int adc_key_in;

  adc_key_in = analogRead( LCD_BTN );
  for( k = 0; k < NUM_KEYS; k++ )
  {
    if ( adc_key_in < adc_key_val[k] )
    {
      return k;
    }
  }
  if ( k >= NUM_KEYS )
    k = -1;
  return k;
}

////////////////////////////////////////////////////////////////////
// Button Press Callbacks - LCD/Keyboard function from vendor
////////////////////////////////////////////////////////////////////

void btn_Pressed_Select()
{
  // toggle LCD brightness
  analogWrite(LCD_BL, (backlightSet = (25 + backlightSet) % 100) );
}

void btn_Pressed_Left()
{
  // Serial.println("Left.");
  // digitalWrite(uP_NMI_N, LOW);
}

void btn_Pressed_Right()
{
  // Serial.println("Right.");
  // digitalWrite(uP_NMI_N, HIGH);
}

void btn_Pressed_Up()
{
  // Serial.println("Up.");
  
  // release uP_RESET
  digitalWrite(uP_RESET, HIGH);
}

void btn_Pressed_Down()
{
  // Serial.println("Down.");
  
  // assert uP_RESET
  digitalWrite(uP_RESET, LOW);
  
  // flush serial port
  while (Serial.available() > 0)
    Serial.read();
}

void process_lcdkeypad()
{
  // Handle key presses
  //
  if ( getKey() ) {
    // button pressed
    if ( BTN_PRESS ) {
      if (key == BTN_SELECT) btn_Pressed_Select();
      if (key == BTN_UP)     btn_Pressed_Up();
      if (key == BTN_DOWN)   btn_Pressed_Down();
      if (key == BTN_LEFT)   btn_Pressed_Left();
      if (key == BTN_RIGHT)  btn_Pressed_Right();      
    }
  } else
   // display processor info & performance
   // if (clock_cycle_count % 10 == 0) 
  {
    char tmp[20];
    float freq;
    
    lcd.setCursor(0, 0);
    // lcd.print(clock_cycle_count);
    sprintf(tmp, "A=%0.4X D=%0.2X", uP_ADDR, DATA_OUT);
    lcd.print(tmp);
    lcd.setCursor(0,1);
    
    freq = (float) (clock_cycle_count - clock_cycle_last) / (millis() - uP_millis_last + 1);
    lcd.print(freq);  lcd.print(" kHz  2650");
    clock_cycle_last = clock_cycle_count;
    uP_millis_last = millis();
  }
}
#endif

////////////////////////////////////////////////////////////////////
// Setup
////////////////////////////////////////////////////////////////////

void setup() 
{

  Serial.begin(115200);

  Serial.write(27);       // ESC command
  Serial.print("[2J");    // clear screen command
  Serial.write(27);
  Serial.print("[H");
  Serial.println("\n");
  Serial.println("Configuration:");
  Serial.println("==============");
  Serial.print("Debug:      "); Serial.println(outputDEBUG, HEX);
  Serial.print("LCD-DISP:   "); Serial.println(USE_LCD_KEYPAD, HEX); 
  Serial.print("SRAM Size:  "); Serial.print(RAM_END - RAM_START + 1, DEC); Serial.println(" Bytes");
  Serial.print("SRAM_START: 0x"); Serial.println(RAM_START, HEX); 
  Serial.print("SRAM_END:   0x"); Serial.println(RAM_END, HEX); 
  Serial.println("");
  Serial.println("=======================================================");
  Serial.println("> PIP BUG");
  Serial.println("> : written by Signetics");
  Serial.println("> ");
  Serial.println("> 2650 Mini System from");
  Serial.println("> Electronics Australia May, 1978");
  Serial.println("=======================================================");
  Serial.println("");
  Serial.println("Enter \\ to send LF");
    
#if (USE_LCD_KEYPAD)
  pinMode(LCD_BL, OUTPUT);
  analogWrite(LCD_BL, backlightSet);  
  lcd.begin(16, 2);
#endif

  // Initialize processor GPIO's
  uP_init();
  k2650_pipbug_init();

  Serial.println("\n");

  // Reset processor
  //
  Serial.println("RESET=1");
  uP_assert_reset();
  for(int i=0;i<25;i++)
    cpu_tick();
  
  // Go, go, go
  uP_release_reset();
  Serial.println("RESET=0");

  pinMode2(7, OUTPUT);
  digitalWrite2(7, LOW);
  pinMode2(6, OUTPUT);
  digitalWrite2(6, HIGH);
}

////////////////////////////////////////////////////////////////////
// Loop()
////////////////////////////////////////////////////////////////////

void loop()
{
  word i = 0;
  word j = 0;
  
  // Loop forever
  //
  while(1)
  {    
    //////////////////////////////
    cpu_tick();
    serialEvent2650();     // handles soft-uart on FLAGE/SENSE.

#define MEASURE_BAUD_RATE 0
#if MEASURE_BAUD_RATE
    if (STATE_FLAG)
    {
      if (j != 0)
      {
        Serial.print("BAUD = "); Serial.println(j);
        j = 0;
      }
    }
    else
    {
      j++;
    }
#endif
      
  // execute lcdkeypad() when word i overflows (simple counter)
#if (USE_LCD_KEYPAD)
    if (i++ == 0) process_lcdkeypad();
#endif
  }
}
