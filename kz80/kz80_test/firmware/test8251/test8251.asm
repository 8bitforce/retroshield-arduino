;-------------------------------------------------
;Z80 DISASSEMBLER LISTING
;Label  Instruction
;-------------------------------------------------
; $0000 - $0100 : ROM
; $8000 - $83FF : RAM
;
; $00 - $01 : 8251 UART

; Code from Mustafa Peker

	.TARGET "Z80"

	; Comma separated 0xAA format for arduino code.
	.setting "OutputFileType", "TXT"
	.setting "OutputTxtAddressFormatFirst", " "
	.setting "OutputTxtAddressFormatNext", " "
	.setting "OutputTxtValueFormat", "0x{0:X02}"
	.setting "OutputTxtValueSeparator", ","
	.setting "OutputTxtLineSeparator", ",\n\r"

	.ORG $0000

	LD SP,$83FF  ; STACK AT THE END OF 8K RAM
	IM 1
	EI

INIT8251:
	LD A,$4D            
	OUT ($01),A      	 ; 8-N-1  CLK/1  4EH FOR /16, 4D FOR /1  (MODE )
	LD A,$37            
	OUT ($01),A      	 ; RESET ALL ERROR FLAGS AND ENABLE RXRDY,TXRDY (COMMAND)

	JP GREET

	.storage $0038-*,$00	; zero until interrupt

	.ORG $0038
INTERRUPT:
	DI			; disable interrupts

	; do interrupt stuff here

	EI			; enable interrupts
	RETI
       

GREET:	
	LD HL,TABLE
	LD B,29
DISP:	
	LD A,(HL)
	CALL TXD
	INC HL
	DJNZ DISP

ECHO0:
	CALL RXD
	CALL TXD
	JP ECHO0

TABLE: 	
	.byte $0A,$0D
	.byte "TXD:    "
	.byte $0A,$0D
	.byte "RXD:   "
	.byte $0A,$0D
	.byte "Ready>"
	.byte $0A,$0D 


;TXD ROUTINE sends contents of A REGISTER  to serial out pin 
;2400 BAUD, 8-N-1

TXD:
	PUSH AF
LOPTX:
	IN A,($01)
	AND $01			;TXRDY?
	JP Z, LOPTX
	POP AF
	OUT ($00),A
	RET

;RXD ROUTINE receives 1 bayt from serial INPUT pin to A REGISTER AND 0F30H adress
;2400 BAUD, 8-N-1

RXD:    
	IN A,($01)
	AND $02		;RXRDY?
	JP Z, RXD
	IN A,($00)	;RECEIVE CHAR
	RET

.end